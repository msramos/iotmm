#include "msp430g2553.h"

#define RXD BIT1
#define TXD BIT2

#define ERROR_LED BIT4
#define OK_LED BIT5

int main(void)
{
   WDTCTL = WDTPW + WDTHOLD; // Stop WDT
   DCOCTL = 0; // Select lowest DCOx and MODx settings
   BCSCTL1 = CALBC1_1MHZ; // Set DCO
   DCOCTL = CALDCO_1MHZ;
   P1SEL |= RXD + TXD ; // P1.1 = RXD, P1.2=TXD
   P1SEL2 |= RXD + TXD ; // P1.1 = RXD, P1.2=TXD
   P1DIR |= OK_LED | ERROR_LED;
   UCA0CTL1 |= UCSSEL_2; // SMCLK
   UCA0BR0 = 0x08; // 1MHz 115200
   UCA0BR1 = 0x00; // 1MHz 115200
   UCA0MCTL = UCBRS2 + UCBRS0; // Modulation UCBRSx = 5
   UCA0CTL1 &= ~UCSWRST; // **Initialize USCI state machine**
   UC0IE |= UCA0RXIE; // Enable USCI_A0 RX interrupt
   P1OUT = 0;
   __bis_SR_register(LPM0_bits + GIE); // Enter LPM0 w/ int until Byte RXe
   while (1)
   { }
}

#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void)
{
  char received_data = UCA0RXBUF;
  P1OUT = 0;
  if(received_data == 'y') {
    P1OUT = OK_LED;
  } else if(received_data == 'n') {
    P1OUT = ERROR_LED;
  }
  UCA0TXBUF = received_data; // retransmit received data to computer
}