require 'serialport'
require 'net/http'

ERROR_URL_LENGTH = 20

code_reader_port = SerialPort.new('/dev/ttyACM1', 9600)
msp_port = SerialPort.new('/dev/ttyUSB0', 115200)

while true
  puts "Waiting for new read. . ."
  student_code = code_reader_port.read(15).chomp
  puts "Student code that was read: #{student_code}"
  puts "Sent request to: \"0.0.0.0/search/#{student_code}\""

  http_respost = Net::HTTP.get("0.0.0.0", "/search/#{student_code}", 3000)

  puts "Received response from server!"
  puts "#{http_respost}"

  redirected_url = http_respost[/http:\/\/0.0.0.0:3000\/.*"/].chomp "\""

  puts "redirected to #{redirected_url}"

  status = redirected_url.length > ERROR_URL_LENGTH

  if status
    puts "Status: Student found!"
    msp_port.write 'y'
  else
    puts "Status: Student not found!"
    msp_port.write 'n'
  end
  msp_respost = msp_port.read 1
  puts "Acknowledge signal from MSP430: #{msp_respost}"
  puts "Transmission completed."
  puts "----------------------------------------------\n\n\n"
end