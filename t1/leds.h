#ifndef LEDS_H
#define LEDS_H

#include "msp430g2553.h"

#define POWER_LED BIT3
#define S0_LED BIT5
#define S1_LED BIT4

#define OK_LED BIT7
#define ERROR_LED BIT6

void blink_1(char led);

void blink_2(char led);

void blink_3(char led);

#endif
