#!ruby

require 'serialport'

CMD_LENGTH = 8
CODE_LENGTH = 15
CODE_OFFSET = CODE_LENGTH - CMD_LENGTH

if ARGV.size != 2
  puts "Usage:"
  puts "\truby <reader> <validator>"
  puts ""
  exit
end

reader_device_name = ARGV[0]
validator_device_name = ARGV[1]

reader_device = SerialPort.new(reader_device_name, 9600)
validator_device = SerialPort.new(validator_device_name, 115200)

puts reader_device
puts validator_device

insert = lambda do
  print 'insert -> '
  code = reader_device.read CODE_LENGTH
  code = "i" + code[0, code.size - 1]
  print "#{code} -> "
  validator_device.write 'i'
  code.each_char { |c| validator_device.write c }
  validator_device.flush_output
  x = validator_device.read 1
  print " code: #{x}. "
end

delete = lambda do
  print 'delete -> '
  code = reader_device.read CODE_LENGTH
  code = "d" + code[0, code.size - 1]
  print "#{code} -> s"
  code.each_char { |c| validator_device.write c }
  validator_device.flush_output
end

erase = lambda do
  print "erase -> "
  validator_device.write "e"
  validator_device.flush_output
end

close = lambda do
  validator_device.close
  reader_device.close
  puts "Bye!"
  exit
end

CMDS = {
  "0987651" => insert,
  "0156789" => delete,
  "0998877" => close,
  "0666666" => erase
}

def authenticate code, validator_device
  print "authenticating #{code} ->"
  code = "a" + code
  print "#{code} -> "
  code.each_char { |c| validator_device.write c }
  validator_device.flush_output
end

while true
  print "Waiting input... "
  cmd = reader_device.read CMD_LENGTH

  if cmd[cmd.size - 1] == "\n"
    cmd = cmd[0 , cmd.size - 1]
  end

  if CMDS.has_key? cmd
    print "admin command: "
    CMDS[cmd].call
  else
    remaining = reader_device.read CODE_OFFSET
    remaining = remaining[0 , remaining.size - 1]
    code = cmd + remaining
    authenticate code, validator_device
  end

  res = validator_device.read 1
  if res == 'k'
    puts 'success :D'
  else
    puts 'failed :('
  end

end
