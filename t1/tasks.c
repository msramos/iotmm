#include "tasks.h"
#include "flash.h"
#include "leds.h"

void task_erase() {
  char *addr = (char*) FLASH_BASE_ADDRESS;
  while(BUSY & FCTL3);                 // Check if Flash being used
  FCTL2 = FWKEY + FSSEL_1 + FN3;       // Clk = SMCLK/4
  FCTL1 = FWKEY + ERASE;               // Set Erase bit
  FCTL3 = FWKEY;                       // Clear Lock bit

  //*addr = 0;                           // Dummy write to erase Flash segment

  int total = MAX_REG * REG_SIZE;
  volatile int i;
  for(i=0; i< total; i++) {
    addr[i] = 0;
  }

  while(BUSY & FCTL3);                 // Check if Flash being used
  FCTL1 = FWKEY;                       // Clear WRT bit
  FCTL3 = FWKEY + LOCK;                // Set LOCK bit

  blink_3(OK_LED);
  UCA0TXBUF = 'k';
}

void task_insert(char* reg) {
  char* next = search_next_free_reg();

  if(next == 0) {
    blink_1(ERROR_LED);
    UCA0TXBUF = 'n';
  }
  else {
    char full = 1;
    before_write();
    char *addr = (char *) FLASH_BASE_ADDRESS;
    *addr = *addr+1;
    after_write();
    write(next, &full, 1);
    write(next+1, reg, REG_SIZE);

    blink_1(OK_LED);
    UCA0TXBUF = 'k';
  }
}

void task_delete(char* reg) {
  char null_reg[15] = {0};
  int index = search_reg(reg);

  if(index >= 0) {
    before_write();
    char *addr = (char *) FLASH_BASE_ADDRESS;
    *addr = *addr-1;
    after_write();

    *addr =(char*) FLASH_BASE_ADDRESS + REG_SIZE * index;
    write(addr, null_reg, 15);
    blink_3(OK_LED);
    UCA0TXBUF = 'k';
  }
  else {
    blink_3(ERROR_LED);
    UCA0TXBUF = 'n';
  }
}

void task_auth(char* reg) {
  int found = search_reg(reg);
  if(found >= 0) {
    blink_2(OK_LED);
    UCA0TXBUF = 'k';
  }
  else {
    blink_2(ERROR_LED);
    UCA0TXBUF = 'n';
  }
}
