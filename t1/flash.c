#include "flash.h"
#include "leds.h"

#include <string.h>
#include <msp430g2553.h>


void blink_1(char led) {
  P1OUT |= led;
  __delay_cycles(100000);
  P1OUT &= ~led;
  __delay_cycles(100000);
  P1OUT |= led;
  __delay_cycles(100000);
  P1OUT &= ~led;
  __delay_cycles(100000);
  P1OUT |= led;
  __delay_cycles(300000);
  P1OUT &= ~led;
}

void blink_3(char led) {
  P1OUT |= led;
  __delay_cycles(100000);
  P1OUT &= ~led;
  __delay_cycles(100000);
  P1OUT |= led;
  __delay_cycles(300000);
  P1OUT &= ~led;
  __delay_cycles(100000);
  P1OUT |= led;
  __delay_cycles(100000);
  P1OUT &= ~led;
}

void blink_2(char led) {
  P1OUT |= led;
  __delay_cycles(300000);
  P1OUT &= ~led;
  __delay_cycles(100000);
  P1OUT |= led;
  __delay_cycles(100000);
  P1OUT &= ~led;
  __delay_cycles(100000);
  P1OUT |= led;
  __delay_cycles(100000);
  P1OUT &= ~led;
}

void before_write() {
  _DINT();                             // Disable interrupts(IAR workbench).
  while(BUSY & FCTL3);                 // Check if Flash being used
  FCTL2 = FWKEY + FSSEL_1 + FN0;       // Clk = SMCLK/4
  FCTL3 = FWKEY;                       // Clear Lock bit
  FCTL1 = FWKEY + WRT;                 // Set WRT bit for write operation
}

void after_write() {
  while(BUSY & FCTL3);                 // Check if Flash being used
  FCTL1 = FWKEY;                        // Clear WRT bit
  FCTL3 = FWKEY + LOCK;                 // Set LOCK bit
  _EINT();
}


void write(char* addr, char* data, int len) {
  before_write();
  int i;
  for (i=0; i<len; i++)
    addr[i] = data[i];         // copy value to flash
  after_write();
}

void read(char *addr, char* data, int len) {
  //before();
  _DINT();
  int i;
  for (i=0; i<len; i++)
    data[i] = addr[i];         // copy value to memory
  _EINT();
  //after();
}

int search_reg(char* reg) {

  char *addr = (char *) FLASH_BASE_ADDRESS;
  unsigned char total = *(addr);
  addr++;

  volatile int reg_index = 0;

  while(reg_index < total) {

    if (memcmp(reg, (addr+1), REG_PAYLOAD) == 0) {
      return reg_index;
    }
    else {
        reg_index++;
        addr += REG_SIZE;
    }
  }
  return -1;
}

char* search_next_free_reg() {
  char *addr = (char *) FLASH_BASE_ADDRESS;
  int total = *(addr);

  if(total >= MAX_REG) {
    UCA0TXBUF = '1';
    blink_2(S0_LED);
    return 0;
  }

  addr++;

  volatile int reg_index = 0;

  while(reg_index < MAX_REG) {

    if (*addr == 0) {
      UCA0TXBUF = '2';
      return addr;
    }
    else {
        reg_index++;
        addr += REG_SIZE;
    }
  }

  UCA0TXBUF = '3';
  blink_1(S1_LED);
  return 0;
}
