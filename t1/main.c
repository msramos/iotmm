#include "msp430g2553.h"
#include "flash.h"
#include "leds.h"
#include "tasks.h"


#define TXD BIT2
#define RXD BIT1

#define ST_WAITING_CMD 1
#define ST_WAITING_CODE 2

volatile int task;
volatile int state = ST_WAITING_CMD;
volatile int pos = 0;
volatile char buffer[14];

int main(void)
{
   WDTCTL = WDTPW + WDTHOLD; // Stop WDT
   DCOCTL = 0; // Select lowest DCOx and MODx settings
   BCSCTL1 = CALBC1_1MHZ; // Set DCO
   DCOCTL = CALDCO_1MHZ;
   P2DIR |= 0xFF; // All P2.x outputs
   P2OUT &= 0x00; // All P2.x reset
   P1SEL |= RXD + TXD ; // P1.1 = RXD, P1.2=TXD
   P1SEL2 |= RXD + TXD ; // P1.1 = RXD, P1.2=TXD
   P1DIR |= S0_LED + S1_LED + POWER_LED + OK_LED + ERROR_LED;
   UCA0CTL1 |= UCSSEL_2; // SMCLK
   UCA0BR0 = 0x08; // 1MHz 115200
   UCA0BR1 = 0x00; // 1MHz 115200
   UCA0MCTL = UCBRS2 + UCBRS0; // Modulation UCBRSx = 5
   UCA0CTL1 &= ~UCSWRST; // **Initialize USCI state machine**
   UC0IE |= UCA0RXIE; // Enable USCI_A0 RX interrupt
   P1OUT = POWER_LED;
   __bis_SR_register(LPM0_bits + GIE); // Enter LPM0 w/ int until Byte RXe
   while (1)
   { }
}

#pragma vector=USCIAB0TX_VECTOR
__interrupt void USCI0TX_ISR(void)
{
  // P1OUT |= TXLED;
  //
  // if (i >= 14) // TX over?
  //   UC0IE &= ~UCA0TXIE; // Disable USCI_A0 TX interrupt
  // else
  //   UCA0TXBUF = string[i++]; // TX next character
  //
  // P1OUT &= ~TXLED;
}

#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void)
{
  P1OUT |= S0_LED;
  switch(state) {

    case ST_WAITING_CMD:
      task = UCA0RXBUF;

      if(task == 'e') {
        task_erase();
      }
      else  {
        state = ST_WAITING_CODE;
      }
      break;

    case ST_WAITING_CODE:
      buffer[pos++] = UCA0RXBUF;
      if(pos >= 14) {
        switch(task) {
          case 'a':
            task_auth(buffer);
            break;
          case 'd':
            task_delete(buffer);
            break;
          case 'i':
            task_insert(buffer);
            break;
        }
        pos = 0;
        state = ST_WAITING_CMD;
      }
      break;
  }
  //__delay_cycles(100000);
  P1OUT &= ~S0_LED;
}
