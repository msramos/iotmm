#include "leds.h"
#include "msp430g2553.h"

void blink_1(char led) {
  P1OUT |= led;
  __delay_cycles(100000);
  P1OUT &= ~led;
  __delay_cycles(100000);
  P1OUT |= led;
  __delay_cycles(100000);
  P1OUT &= ~led;
  __delay_cycles(100000);
  P1OUT |= led;
  __delay_cycles(300000);
  P1OUT &= ~led;
}

void blink_2(char led) {
  P1OUT |= led;
  __delay_cycles(100000);
  P1OUT &= ~led;
  __delay_cycles(100000);
  P1OUT |= led;
  __delay_cycles(300000);
  P1OUT &= ~led;
  __delay_cycles(100000);
  P1OUT |= led;
  __delay_cycles(100000);
  P1OUT &= ~led;
}

void blink_3(char led) {
  P1OUT |= led;
  __delay_cycles(300000);
  P1OUT &= ~led;
  __delay_cycles(100000);
  P1OUT |= led;
  __delay_cycles(100000);
  P1OUT &= ~led;
  __delay_cycles(100000);
  P1OUT |= led;
  __delay_cycles(100000);
  P1OUT &= ~led;
}
