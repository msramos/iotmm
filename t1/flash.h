#ifndef FLASH_H
#define FLASH_H

#define FLASH_BASE_ADDRESS 0x0E000
#define MASTER_OFFSET 0

#define MAX_REG 100

#define REG_HEADER 1
#define REG_PAYLOAD 14
#define REG_SIZE REG_HEADER + REG_PAYLOAD

void before_write();

void after_write();

void write(char *pos, char* data, int len);

void read(char *pos, char* data, int len);

int search_reg(char* reg);

char* search_next_free_reg();

#endif
