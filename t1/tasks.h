#ifndef TASKS_H
#define TASKS_H

void task_erase();

void task_insert(char* reg);

void task_delete(char* reg);

void task_auth(char* reg);

#endif
